<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page 
    language="java" 
    contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

    <head>
        <title>Microcloudnet</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />

        <meta http-equiv="refresh" content="5" > 
	
        <!-- Layout Stylesheet -->
        <link rel="stylesheet" type="text/css" href="style/style.css" />
        <!-- Colour Scheme Stylesheet -->
        <link rel="stylesheet" type="text/css" href="style/colour.css" />

        <script language="JavaScript" src="scripts/main.js"></script>
    </head>

    <%@ page import="java.text.*,java.util.*" %>
    <%@ page import="src.RCBean.*" %>

    <body onload="setSelectedMenuItem('home')">
        <div id="main">
            <!-- import header -->
            <%@ include file="header.html"%>

            <div id="content">
                <div id="column1">
                    <div id="menu">
                        <%@ include file="main_menu.jsp" %>
                    </div>
                </div> <!-- end column1 -->
                
                <div id="column2">
                    <br/><br/>
                    <h2><font color="blue">Microcloudnet.</font></h2>
                    <br/><br/>
                    <!-- INSERT PAGE CONTENT HERE -->

		    <%
		    final SimpleDateFormat sdf = new SimpleDateFormat("'Hora atual:' HH:mm:ss");
		    %>
		    <h3>Execu��o iniciada</h3>
		    <br/><br/>
		    <h3><%=sdf.format(new Date()) %></h3>

                </div> <!-- end column2 -->
            </div> <!-- end content -->
   
            <!-- import footer -->
            <%@ include file="footer.html"%>

        </div> <!-- end main -->
    </body>
</html>
