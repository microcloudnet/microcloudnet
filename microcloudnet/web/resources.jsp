<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1" %>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>REALcloud Web Lab</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<!--        <meta http-equiv="refresh" content="5" > -->
<!--        <% if (request.getParameter("sync").equals("true")){ %>
           window.onload=initSync;
	<% }//fim if %>
-->
<script type="text/javascript">
  var http;
  var SHAREABLE;

  function getHTTPObject(){
	if (window.ActiveXObject) 
		return new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest) 
		return new XMLHttpRequest();
	else {
		alert("Your browser does not support AJAX.");
		return null;
	}
      }

      function manageShare(resourceName){

         var listShare = document.getElementById("listShare"+resourceName);
         SHAREABLE = listShare.options[listShare.selectedIndex].value;
         //window.alert(resourceName + ' ' + SHAREABLE);

 	 httpObject = getHTTPObject();
	 if (httpObject != null) {
            httpObject.open("GET", "resources.jsp?action=share&sync=false&resourceName=" + resourceName + "&shareable=" + SHAREABLE,true);
	    httpObject.send(null);
            //setDiv nao aceita argumentos
	    httpObject.onreadystatechange = setDiv;
	}//fim if
	
      }//fim manageShare

function setDiv(){
	if(httpObject.readyState == 4){
           //window.alert(httpObject.responseText);
           window.alert('Compartilhado: ' + SHAREABLE);
           window.location.reload();
	}//fim if
}//fim setDiv

//--Para o VNC no browser

 function wresize() {

   var ps;
   try { 
     ps = document.vncapp.getPreferredSize();
   } catch (e) {
     setTimeout ("wresize()", 100);
     return;
   }

   var aw = ps.width;
   var ah = ps.height;
   var oh;
   var ow;

   if (!window.innerHeight && document.body && document.body.offsetHeight) {
     // hack for IE
     oh = document.documentElement.clientHeight;
     ow = document.documentElement.clientWidth;
   }  else {
     // other browsers
     oh = window.innerHeight;
     ow = window.innerWidth;
   }

   document.vncapp.style.height = ah + "px";
   document.vncapp.style.width = aw + "px";

   var offsetw = aw - ow + 2;
   var offseth = ah - oh + 2;


   if (offsetw !== 0 || offseth !== 0) {
     try { window.resizeBy(offsetw, offseth); } catch (e) {}
   }

   setTimeout ("wresize()", 1000);
 }

    </script>
        <!-- Layout Stylesheet -->
        <link rel="stylesheet" type="text/css" href="./style/style.css" />
        <!-- Colour Scheme Stylesheet -->
        <link rel="stylesheet" type="text/css" href="./style/colour.css" />
        <script language="JavaScript" src="./scripts/main.js">
        </script>
    </head>
    <%@ page import="java.io.*,java.net.*,java.util.*,java.util.Date" %>
    <%@ page import="java.sql.*" %>
    <%@ page import="java.awt.*,java.awt.event.*" %>
    <%@ page import="src.LabBean.*" %>

    <body onload="setSelectedMenuItem('resources')">
        <div id="main">
            <!-- import header --><%@ include file="./header.html" %>
            <div id="content">
                <div id="column1">
                    <div id="menu">
                        <%@ include file="./main_menu.jsp" %>
                    </div>
                    <!-- End menu -->
                </div>
                <!-- End column1 -->
                <div id="column2">

                    <!-- INSERT PAGE CONTENT HERE -->
<%
//String tipoHTTP=http;
String tipoHTTP="https";

String SID=request.getParameter("SID");
String currentUser=request.getParameter("currentUser");

%>
<br>
<br>
<!-- <td><p>User: <%=currentUser%>.</td> -->

			<!--<form method="post" href="#" action="resources.jsp?action=logout&sync=false&SID=<%=SID%>&currentUser=<%=currentUser%>">-->
			<form method="post" href="#" action="index.jsp">
                          <table>
			    <tr>
			      <td><input type="submit" value="Logout"></td>
                              </tr>
			    </table>
			  </form>
			  <br>

<!--                    <h1>Resource Management</h1>                    -->
                    <!-- INSERT PAGE CONTENT HERE -->
		  
 		          <!-- Inicio do form -->
<!--                          <form name="formSync" action="resources_sync.jsp?action=list" method="post">
			  <table border="1">
			    <tr>
			      <font face="helvetica" size="-1">
				<br/>
				<br/>
-->
<!--				<b>Session Validation</b>
			      </font>
                              <br/>
			      <br/>
			      <td>
				<table>
-->
				  <!-- hidden define um nome de campo ah mais, no caso,
				       o nome eh action, mas esse campo nao aparece no browser
				       Para atribuir o seu valor dentro do form: form.action.value
				    -->	
<!--
				  <tr>
				    <td> <input class="submit" type="submit" value="Validate">
				    </td>
				  </tr>
				  <tr>
				      <td>Session ID:</td>
				      <td>
				      <input class="text" type="text" id="idGlobal" maxLength=128 size=64 name="idGlobal" value="SID_TESTE@https://143.106.148.167:9443/"/>
				      </td>
				  </tr>
				  <tr>
				      <td>VM:</td>
				      <td>
				      <input class="text" type="text" id="privateUrl" maxLength=128 size=64 name="privateUrl" value="Ubuntu_VM_i386_2"/>
				      </td>
				  </tr>
			        </table>
			       </td>
			      </tr>
			   </table>
                          </form>
-->
			  <!-- Fim do form -->

		    <%
                    //Variaveis globais
                    ResourceBean rBean = new ResourceBean();                    
                    String action = request.getParameter("action");

        //Logout
        if (action.equals("logout")){        
        
        String URL_consultaFederacao = "http://realcloud.dca.fee.unicamp.br:8000/RealCloud/SIDService?action=logout&SID=" + SID + "&currentUser=" + currentUser;
		       SID="";
		       currentUser="";

            String respostaHTTPFederacao="";
            try {

                URL netUrlFederacao = new URL(URL_consultaFederacao);

                HttpURLConnection connectionFederacao = null;

                connectionFederacao = (HttpURLConnection)netUrlFederacao.openConnection();

                connectionFederacao.setRequestMethod("GET");
                connectionFederacao.setUseCaches(false);
                connectionFederacao.setDoInput(true);
                connectionFederacao.setDoOutput(true);
                connectionFederacao.setReadTimeout(15000);

                BufferedReader readerFederacao = new BufferedReader(new InputStreamReader(connectionFederacao.getInputStream()));
                String lineFederacao = readerFederacao.readLine();
                while( null != lineFederacao ) {
                    respostaHTTPFederacao += lineFederacao;
                    lineFederacao = readerFederacao.readLine();
                }//fim while   

                readerFederacao.close();

            } catch (Exception e) {
                respostaHTTPFederacao = "Excecao ao acessar SIDService: " + e.getMessage();
            }//fim catch               

        } else 

        // Share a Resource
        if ( action.equals("share")) {

           String resourceName = request.getParameter("resourceName");
           String shareable = request.getParameter("shareable");
           rBean.setName(resourceName);           
          
          //Recupero todas as variaveis para esse resourceName
          if( rBean.retrieveFromDB() == false )
            throw new Exception("No resource in request");
          else {
             //Atualiza o campo
             rBean.setShareable(shareable);
             if( rBean.updateIntoDB() == false )
                throw new Exception("Error updating resource into DB");
             else 
                response.sendRedirect("resources.jsp?action=list&sync=false");
          }//fim else

        }//fim if

                    // List all Resources
                    if( action != null && action.equals("list")) { %>
                    
                    <table>

                    <tr>
<!--                    <td>
                    <a class="framed" href="#" onClick="javascript:window.location='resources.jsp?action=list&sync=false'">Reload Status</a>
                    </td>
-->
                    </tr>

<!--		    <tr>
                    <td>		      
		      <font face="helvetica" size="-1">
			<b>Permite o acesso �s VMs com NAT: (Ex.: IP_publico:porta) </b>
		      </font>
		    </td>
		    </tr>

		    <tr>
		    <td>
                    <a class="framed" href="#" onClick="javascript:window.location='NetworkManager?action=start'">Start Iptables</a>
                    </td>
		    </tr>

		    <tr>
                    <td>
		      <font face="helvetica" size="-1">
			<b>Flush do Iptables:</b>
		      </font>
		    </td>
		    </tr>

		    <tr>
		    <td>
                    <a class="framed" href="#" onClick="javascript:window.location='NetworkManager?action=stop'">Stop Iptables</a>
                    </td>
		    </tr>
-->
                    </table>

	            <font face="helvetica" size="-1">
                    <b>VM List</b>
		    </font>

                    <br/>  

                    <table id="hor-zebra">
                    <form name="f" action="resources.jsp" method="post">
                        <%	ArrayList list = rBean.retrieveAll();
                        Iterator it = list.iterator();
                        //Para exibir uma cor de linha diferente
                        int cor=0;
                        while (it.hasNext()){                                               
                        ResourceBean resource = (ResourceBean) it.next(); 
                        if (resource.getOwner().equals(currentUser)){
                        if (cor==0) {
                           cor=1;
                        %>
                        <tr class="odd">
                        <% } else {
                             cor=0;
                        %>
                        <tr>
                        <%
                        }//fim else
                        %>
                            <td> 
                                <%=resource.getName() %>
                            </td>
                            <td>
                                <%=resource.getOwner() %>
                            </td>
                            <td>
                                <a class="framed" href="#" onClick="javascript:window.location='VNCService?tipoHTTP=<%=tipoHTTP%>&VMID=<%=resource.getName()%>'">VNC</a>
                            </td>

                            <td>
                                <a class="framed" href="#" onClick="javascript:window.location='VMManager?tipoHTTP=<%=tipoHTTP%>&action=start&resourceName=<%=resource.getName()%>&SID=<%=SID%>&currentUser=<%=currentUser%>'">Start</a>
                            </td>
                            <td>
                                <a class="framed" href="#" onClick="javascript:window.location='VMManager?tipoHTTP=<%=tipoHTTP%>&action=shutdown&resourceName=<%=resource.getName()%>&SID=<%=SID%>&currentUser=<%=currentUser%>'">Stop</a>
                            </td>

                            <td>
                                <a class="framed" href="#" onClick="javascript:window.location='resources_details.jsp?<%=tipoHTTP%>&action=details&resourceName=<%=resource.getName()%>&SID=<%=SID%>&currentUser=<%=currentUser%>'">Details</a>
                            </td>
                            </tr>
                            <%
			       }//fim if
                            } //end while %>
                            </form>
                            </table>
                            <%
                            } // End if %>

                            <br/>
                            <br/>

  			    <font face="helvetica" size="-1">
                            <b>Status of VMs</b>
			    </font>
			    <br/>

<font face="helvetica" size="2">
<pre>
<%=rBean.getStatus(currentUser)%>
</pre>
</font>

                        </div>
                        <!-- End of Column 2 -->
                        </div>
                        <!-- End of Content -->
                        <!-- import footer -->
                        <%@ include file="../footer.html" %>
                    </div>
                    <!-- End of Main -->

                    </body>
                </html>
