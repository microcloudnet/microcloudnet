<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page 
    language="java" 
    contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    isErrorPage="true"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

  <head>
    <title>Microcloudnet - Error Page</title>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  </head>

  <body>
	  <h2>Error Report</h2>

The following error was detected during the processing of your request:
<br>
<br>
<font color="red">
<%
Exception e = (Exception)request.getAttribute("javax.servlet.error.exception");
out.println(e.getMessage());
%>
</font>
</body>
</html>
