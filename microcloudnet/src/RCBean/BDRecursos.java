package src.RCBean;

public class BDRecursos {

	//Colunas da BD
	private String id;
	private String nome;
	private String identidade;
	private String localizacao;
	private String atividade;
	private String tempo;
	private String compartilhado;
	private String contabilidade;
	
	public BDRecursos() {
		super();
	}//fim construtor

	public void setID(String valor){
		id = valor;
	}//fim setID
	
	public void setNome(String valor){
		nome = valor;
	}//fim setNome
	
	public void setIdentidade(String valor){
		identidade = valor;
	}//fim setIdentidade
	
	public void setLocalizacao(String valor){
		localizacao = valor;
	}//fim setLocalizacao
	
	public void setAtividade(String valor){
		atividade = valor;
	}//fim setAtividade
	
	public void setTempo(String valor){
		tempo = valor;
	}//fim setTempo	
	
	public void setCompartilhado(String valor){
		compartilhado = valor;
	}//fim setCompartilhado
	
	public void setContabilidade(String valor){
		contabilidade = valor;
	}//fim setContabilidade	
	
	public String getID(){
		return id;
	}//fim getID
	
	public String getNome(){
		return nome;
	}//fim getNome
	
	public String getIdentidade(){
		return identidade;
	}//fim getIdentidade
	
	public String getLocalizacao(){
		return localizacao;
	}//fim getLocalizacao
	
	public String getAtividade(){
		return atividade;
	}//fim getAtividade
	
	public String getTempo(){
		return tempo;
	}//fim getTempo	
	
	public String getCompartilhado(){
		return compartilhado;
	}//fim getCompartilhado
	
	public String getContabilidade(){
		return contabilidade;
	}//fim getContabilidade	
	
	public void setValor(int i, String valor){
		
		switch (i) {
			
			case 1: 
				setID(valor);
				break;
			case 2:
				setNome(valor);
				break;
			case 3:
				setIdentidade(valor);
				break;
			case 4:
				setLocalizacao(valor);
				break;
			case 5:
				setAtividade(valor);
				break;
			case 6: 
				setTempo(valor);
				break;
			case 7:
				setCompartilhado(valor);
				break;
			case 8:
				setContabilidade(valor);
				break;
				
		}//fim switch
		
	}//fim setValor	
	
	public String getValor(int i){
		
		String resultado="";
		
		switch (i) {
			
			case 1: 
				resultado = getID();
				break;
			case 2:
				resultado = getNome();
				break;
			case 3:
				resultado = getIdentidade();
				break;
			case 4:
				resultado = getLocalizacao();
				break;
			case 5:
				resultado = getAtividade();
				break;
			case 6: 
				resultado = getTempo();
				break;
			case 7:
				resultado = getCompartilhado();
				break;
			case 8:
				resultado = getContabilidade();
				break;
				
		}//fim switch
		
		return resultado;
		
	}//fim getValor	
	
}//fim classe
