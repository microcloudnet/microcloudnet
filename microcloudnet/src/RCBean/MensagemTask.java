
package src.RCBean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;

public class MensagemTask extends TimerTask {
    
    private final Date currentTime = new Date();
    private final SimpleDateFormat sdf = new SimpleDateFormat("'Hora:' HH:mm:ss");

    private String getTime(){
	return "Tempo";
    }
    
    @Override
    public void run() {
        currentTime.setTime(System.currentTimeMillis());
        sdf.format(currentTime);
    }
    
}
