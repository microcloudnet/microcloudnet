package src.RCBean;

/**
 * Projeto: Microcloudnet
 * 
 * Arquivo: Constantes.java
 * 
 * Descricao: Classe que guarda os nomes das 
 * colunas da base de dados
 * 
 * @author Lucio Agostinho Rocha
 * 
 * Ultima atualizacao: 03/12/2016
 *
 */

public class Constantes {

	public Constantes(){
		
		super();
		
	}//fim construtor
	
	public static String [] colunasUsuarios = { "ID", "Name", "Email", "Password" };
	public static String [] colunasRecursos = { "ID", "Name", "Identity", "Location", "Activity", "Time", "Shared", "Billing" };
	
	
}//fim classe
