package src.RCBean;

public class BDUsuarios {

	//Colunas da BD
	private String id;
	private String nome;
	private String email;
	private String senha;
	
	public BDUsuarios() {
		super();
	}//fim construtor

	public void setID(String i){
		id = i;
	}//fim setID
	
	public void setNome(String n){
		nome = n;
	}//fim setNome
	
	public void setEmail(String e){
		email = e;
	}//fim setEmail	
	
	public void setSenha(String s){
		senha = s;
	}//fim setSenha
	
	public String getID(){
		return id;
	}//fim getID
	
	public String getNome(){
		return nome;
	}//fim getNome
	
	public String getEmail(){
		return email;
	}//fim getEmail	
	
	public String getSenha(){
		return senha;
	}//fim getSenha	
	
	public void setValor(int i, String valor){
		
		switch (i) {
			
			case 1: 
				setID(valor);
				break;
			case 2:
				setNome(valor);
				break;
			case 3:
				setEmail(valor);
				break;
			case 4:
				setSenha(valor);
				break;
		
		}//fim switch
		
	}//fim setValor	
	
	public String getValor(int i){
		
		String resultado="";
		
		switch (i) {
			
			case 1: 
				resultado = getID();
				break;
			case 2:
				resultado = getNome();
				break;
			case 3:
				resultado = getEmail();
				break;
			case 4:
				resultado = getSenha();
				break;
				
		}//fim switch
		
		return resultado;
		
	}//fim getValor
	
}//fim classe
