/**
 * Projeto Microcloudnet
 *
 * Descricao: Classe que consulta, armazena e recupera da BD
 * 
 * Ultima modificacao: 03/12/2016
 * 
 * @author Lucio Agostinho Rocha
 */

package src.RCBean;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;


public class RCBean {

	private static final long serialVersionUID = 1L;

	//Variaveis de instancia das consultas SQL
	//conexao com o BD
	private Connection c = null;

	//declaracao - recebe a atribuicao da conexao c
	//com o BD para realizar as declaracoes SQL
	private Statement s = null;

	//recolhe as consultas SQL
	private ResultSet rs = null;

	//Instrucoes SQL
	private String sql;

	//Objeto que faz a conexao com o BD
	private BDConexao conexaoBD;

	public RCBean() {
		//Eh necessario ter um construtor aqui para
		//invocar o metodo construtor da superclasse
		super();		
		
/*		ResourceBean resource = new ResourceBean();
		
		ArrayList resources = resource.retrieveAll();
		Iterator it = resources.iterator();
		while (it.hasNext()){

		    ResourceBean recurso = (ResourceBean) it.next();
		    System.out.println(recurso.getName());
		    
		}//fim while
*/		
		
/*		ArrayList lista = getUsuarios();
		Iterator it = lista.iterator();
		
		while (it.hasNext()){
			
		    BDUsuarios usuario = (BDUsuarios) it.next();
		    System.out.println(usuario.getNome());
		    
		}//fim while
*/		
		//System.out.println(verificarUsuarioSenha("outrosdiasvirao@yahoo.com.br", "123"));
		
	}//fim construtor


	public static void main (String args[]){
		
		new RCBean();
		
	}//fim main	

	
	public Boolean verificarUsuarioSenha(String email, String senha){
		
		Boolean registrado=false;
		
		if ( conectarBD() ){
		
			try {
			//Recolhe todos os dados da configuracao na base de dados
			sql = sql_queryEmailSenha(email, senha);	          
			//Consulta a base de dadosBAR
			rs = s.executeQuery( sql );

			if (rs.next())
				registrado=true;
			} catch (Exception e){
				e.printStackTrace();
			}//fim catch
			
			//Encerra os recursos da conexao com a base de dados
			fecharRecursosBD();
			
		} else 
			System.out.println("Erro ao realizar a conexao com a BD.");
		
		return registrado;
		
	}//fim verificarEmailSenha
	
	public ArrayList getUsuarios(){

		ArrayList lista = null;

		if ( conectarBD() ){

			lista = new ArrayList();
			BDUsuarios usuario;

			try {
				//Recolhe todos os dados da configuracao na base de dados
				sql = sql_queryUsuarios();	          
				//Consulta a base de dadosBAR
				rs = s.executeQuery( sql );

				int coluna=1;
				while (rs.next()) {

					//Importante: COLUNAS no MySQL comecam com indice 1
					coluna=1;
					usuario = new BDUsuarios();
					
					while (coluna <= Constantes.colunasUsuarios.length )
						usuario.setValor(coluna, (String) rs.getObject(coluna++));
					
					lista.add(usuario);
					
				}//fim while

				//Encerra os recursos da conexao com a base de dados
				fecharRecursosBD();

			} catch (Exception e) {
				e.printStackTrace(); 
			}//fim catch

		} else 			
			System.out.println("Erro ao realizar a conexao com a BD.");		

		return lista;

	}//fim getUsuarios
	
	public ArrayList getRecursos(){

		ArrayList lista = null;

		if ( conectarBD() ){

			lista = new ArrayList();
			BDRecursos recurso;

			try {
				//Recolhe todos os dados da configuracao na base de dados
				sql = sql_queryRecursos();	          
				//Consulta a base de dadosBAR
				rs = s.executeQuery( sql );

				int coluna=1;
				while (rs.next()) {

					//Importante: COLUNAS no MySQL comecam com indice 1
					coluna=1;
					recurso = new BDRecursos();
					
					while (coluna <= Constantes.colunasRecursos.length )
						recurso.setValor(coluna, (String) rs.getObject(coluna++));
					
					lista.add(recurso);
					
				}//fim while

				//Encerra os recursos da conexao com a base de dados
				fecharRecursosBD();

			} catch (Exception e) {
				e.printStackTrace(); 
			}//fim catch

		} else 			
			System.out.println("Erro ao realizar a conexao com a BD.");		

		return lista;

	}//fim getRecursos	
	
	public String sql_queryUsuarios(){

		return "SELECT * FROM Usuarios ORDER BY Nome";

	}//fim sql_queryUsuarios
	
	public String sql_queryEmailSenha(String email, String senha){
		
		return "SELECT * FROM Usuarios WHERE " + 
		       "Email='" + email + "' AND " +
		       "Senha='" + senha + "'";
		
	}//fim sql_queryEmailSenha
	
	public String sql_queryRecursos(){

		return "SELECT * FROM Recursos ORDER BY ID";

	}//fim sql_queryRecursos

	public boolean conectarBD(){

		conexaoBD = new BDConexao();

		boolean conectou = conexaoBD.conectarBD();
		//Se conectou, adquire as referencias
		if (conectou){
			c = conexaoBD.getC();
			s = conexaoBD.getS();
			rs = conexaoBD.getRS();
		}//fim if

		return conectou;

	}//fim conectarBD

	public void fecharRecursosBD(){

		conexaoBD.fecharConexaoBD(c,s,rs);

	}//fecharRecursosBD

	public String exibirErrosSQL(SQLException e) {

		return "\nSQLException: " + e.getMessage() + 
		"\nSQLState:     " + e.getSQLState() +
		"\nVendorError:  " + e.getErrorCode();

	}//fim exibirErrosSQL

	public String tratamentoExcecao( int numeroExcecao, SQLException e ){

		String descricaoExcecao = "";

		switch ( numeroExcecao ){

		case 1:
			descricaoExcecao = "Nao foi possivel inserir a configuracao na BD.";
			break;

		case 2:
			descricaoExcecao = "Nao foi possivel remover a configuracao da BD.";
			break;

		case 3:
			descricaoExcecao = "Nao foi possivel recolher a configuracao da BD.";
			break;

		case 4:
			descricaoExcecao = "Nao foi possivel consultar o registro da BD.";
			break;		      

		case 5:
			descricaoExcecao = "Nao foi possivelatualizar o registro na BD.";
			break;		  		  

		case 6:
			descricaoExcecao = "Erro no thread de atualizacao.";
			break;

		case 7:
			descricaoExcecao = "Nao foi possivel ler todos os registros recolhidos";
			break;

		}//fim switch

		String mensagemExcecao = "Excecao " + numeroExcecao + ": " + descricaoExcecao + "\n" + exibirErrosSQL(e); 

		return mensagemExcecao;

	}//fim tratamentoExcecao
	
}//fim classe
